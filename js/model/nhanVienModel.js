function NhanVien (
    _taiKhoan,
    _hoTen,
    _email,
    _matKhau,
    _ngayLam,
    _luong,
    _chucVu,
    _gioLam
  ) {
    this.taiKhoan = _taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luong = _luong;
    this.chucVu = _chucVu;
    this.gioLamViec = _gioLamViec;
  
    this.tinhTongLuong = function () {
      if (this.chucVu == "1") {
        return this.luong * 3;
      } else if (this.chucVu == "2") {
        return this.luong * 2;
      } else return this.luong * 1;
    };
  
    this.xepLoaiNV = function() {
      if (this.gioLamViec >= 192) {
        return "Nhân viên xuất sắc";
      } else if (this.gioLamViec >= 176) {
        return "Nhân viên giỏi";
      } else if (this.gioLamViec >= 160) {
        return " Nhân viên khá";
      } else {
          return "Nhân viên trung bình";
      }
    };
  };
  