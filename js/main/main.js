let danhSachNhanVien = [];

let validationNV = new Validator();

//Lưu LOCAL-STORAGE
let DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
function luuLocalStorage() {
  let dsnvJson = JSON.stringify(danhSachNhanVien);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
}

//lấy dữ liệu từ localstorage khi user tải lại trang
const renderDanhSachNhanVien = function () {
  let dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
  // Gán cho array gôc và render lại giao diện
  if (dsnvJson) {
    danhSachNhanVien = JSON.parse(dsnvJson);
    danhSachNhanVien = danhSachNhanVien.map(function (item) {
      return new NhanVien(
        item.taiKhoan,
        item.hoTen,
        item.email,
        item.matKhau,
        item.ngayLam,
        item.luong,
        item.chucVu,
        item.gioLam
      );
    });
    showDanhSachNhanVien(danhSachNhanVien);
  }
};
renderDanhSachNhanVien();
function timKiem(id, array) {
  return array.findIndex(function (item) {
    return item.account == id;
  });
}

//reset lại modal thêm nhân viên
document.getElementById("btnThem").addEventListener("click", () => {
  document.getElementById("form_NV").reset();
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
});

let isUpdate = false;
let checkValidation = function () {
  isValidTen =
    validationNV.kiemTraRong("name", "tbTen") && validationNV.checkLetters();
  isValidEmail =
    validationNV.kiemTraRong("name", "tbEmail") &&
    validationNV.kiemTraEmail();
  isValidMatKhau =
    validationNV.kiemTraRong("password", "tbMatKhau") &&
    validationNV.kiemTraMatKhau();
  isValidNgayLam =
    validationNV.kiemTraRong("datepicker", "tbNgay") &&
    validationNV.kiemTraNgay();
  isValidLuong =
    validationNV.kiemTraRong("luongCB", "tbLuongCB") &&
    validationNV.kiemTraLuong();
  isValidChucVu = validationStaff.kiemTraChucVu();
  isValidGioLam =
    validationNV.kiemTraRong("gioLam", "tbGiolam") &&
    validationNV.kiemTraGioLam();
  if (isUpdate) {
    isValidTaiKhoan =
      validationNV.kiemTraRong("tknv", "tbTKNV") && validationNV.kiemTraID();
  } else {
    isValidTaiKhoan =
      validationNV.kiemTraRong("tknv", "tbTKNV") &&
      validationNV.kiemTraID() &&
      validationNV.kiemTraTrungID();
  }
  isValid =
    isValidTaiKhoan &&
    isValidTen &&
    isValidEmail &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam;
};

//thêm
document.getElementById("btnThemNV").addEventListener("click", function () {
  isUpdate = false;
  checkValidation();
  if (isValid) {
    let newNV = layThongTinTuform();
    danhSachNhanVien.push(newNV);
    luuLocalStorage();
    showDanhSachNhanVien(danhSachNhanVien);
  }
});

//xóa
function xoaNhanVien(id) {
  let viTri = timKiem(id, danhSachNhanVien);

  danhSachNhanVien.splice(viTri, 1);
  renderDanhSachNhanVien();
  luuLocalStorage();
}

//sửa
function suaNhanVien(id) {
  document.getElementById("tknv").disable = true;
  let viTri = timKiem(id, danhSachNhanVien);
  let nhanVien = danhSachNhanVien[viTri];

  xuatThongTinLenForm(nhanVien);
}

//cập nhật
document.getElementById("btnCapNhat").addEventListener("click", function () {
  isUpdate = true;
  checkValidation();
  if (isValid) {
    let nhanVienEdit = layThongTinTuForm();
    danhSachNhanVien.push(nhanVienEdit);
    luuLocalStorage();
    showDanhSachNhanVien(danhSachNhanVien);
  }
});

//tim kiem theo loai
document.getElementById("btnTimNV").addEventListener("click", function () {
  let danhSachTimNV = [];
  let nhanVienCanTim = document.getElementById("searchName").value.trim();

  danhSachNhanVien.forEach((item) => {
    if (item.xepLoaiNV() == nhanVienCanTim) {
      danhSachTimNV.push(item);
    }
    renderDanhSachNhanVien(danhSachTimNV);
  });
});

//hien thi lai
function hienThi() {
  let timKiemten = document.getElementById("searchName").value;
  if (timKiemten == "") {
    renderDanhSachNhanVien();
  }
}
