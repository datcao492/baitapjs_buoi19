function Validator() {
  this.kiemTraRong = function (idTarget, idErr) {
    var valueInput = document.getElementById(idTarget).value.trim();

    if (valueInput) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText = "Không được để trống";
      return false;
    }
  };

  this.kiemTraID = function () {
    let input = document.getElementById("tknv").value;
    if (input.length >= 4 && input.length <= 6) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    } else {
      document.getElementById("tbTKNV").innerText = "ID từ 4-6 ký tự";
      return false;
    }
  };

  this.kiemTraTrungID = function () {
    let input = document.getElementById("tknv").value;
    let viTri = danhSachNhanVien.findIndex(function (item) {
      return item.taiKhoan == input;
    });
    if (viTri == -1) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    } else {
      document.getElementById("tbTKNV").innerText = "ID bị trùng";
      return false;
    }
  };

  this.kiemTraEmail = function () {
    let parten = document.getElementById("email").value;
    let kiemTra = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;

    if (parten.match(kiemTra)) {
      document.getElementById("tbEmail").innerText = "";
      return true;
    } else {
      document.getElementById("tbEmail").innerText = "Email không hợp lệ";
      return false;
    }
  };

  this.kiemTraMatKhau = function () {
    let kiemtraPW = /^(?=.*?[0-9])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{6,10}$/;
    let parten = document.getElementById("password").value;

    if (parten.match(kiemtraPW)) {
      document.getElementById("password").innerText = "";
      return true;
    } else {
      document.getElementById("tbMatKhau").innerText =
        "Password phải có từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
      return false;
    }
  };

  this.kiemTraLuong = function () {
    let input = document.getElementById("luongCB").value;

    if (input >= 1e6 && input <= 20e6) {
      document.getElementById("tbLuongCB").innerText = "";
      return true;
    } else {
      document.getElementById("tbLuongCB").innerText =
        "Lương phải từ 1tr đến 20tr";
      return false;
    }
  };

  this.kiemTraGioLam = function() {
    let input = document.getElementById("gioLam").value;

    if (input >= 80 && input <= 200) {
      document.getElementById("tbGiolam").innerText = "";
      return true;
    } else {
      document.getElementById("tbGiolam").innerText =
        "Giờ làm phải từ 80h - 200h/tháng";
        return false;
    }
  };

  this.kiemTraNgay = function () {
    let date = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    let input = document.getElementById("datepicker").value;
    if (date.test(input)) {
      document.getAnimations("tbNgay").innerText = "";
      return true;
    } else {
      document.getElementById("tbNgay").innerText = "Ngày định dạng mm/dd/yyyy";
      return false;
    }
  };

  this.kiemTraTen = function () {
    let input = document.getElementById("name").value;
    let letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (letters.test(input)) {
      document.getElementById("tbTen").innerText = "";
      return true;
    } else {
      document.getElementById("tbTen").innerText = "Tên không hợp lệ";
      return false;
    }
  };

  this.kiemTraChucVu = function () {
    let input = document.getElementById("chucvu").value;

    if (input == "1" || input == "2" || input == "3") {
        document.getElementById("inform").innerText = "";
        return true;
      } else {
        document.getElementById("inform").innerText = "Vui lòng chọn chức vụ";
        return false;
      }
    };
}


