function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let hoTen = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let matKhau = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luong = document.getElementById("luongCB").value * 1;
  let chucVu = document.getElementById("chucvu").value;
  let gioLam = document.getElementById("gioLam").value * 1;

  return (newStaff = new Staff(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luong,
    chucVu,
    gioLam
  ));
}

function renderDanhSachNhanVien(dsnv) {
  let contentHTML = "";

  for (let index = 0; index < dsnv.length; index++) {
    let nhanVien = dsnv[index];

    let chucVuNhanVien = nhanVien.chucVu;
    function chonChucVu() {
      if (chucVuNhanVien == "1") {
        return "Sếp";
      } else if (chucVuNhanVien == "2") {
        return "Trưởng Phòng";
      } else {
        return "Nhân viên";
      }
    }

    let contentTr = `<tr>
          <td>${nhanVien.taiKhoan}</td>
          <td>${nhanVien.hoTen}</td>
          <td>${nhanVien.email}</td>
          <td>${nhanVien.ngayLam}</td>
          <td>${nhanVien.chonChucVu()}</td>
          <td>${nhanVien.luong}</td>
          <td>${nhanVien.gioLam}</td>
          <td>${nhanVien.tinhTongLuong()}</td>
          <td>${nhanVien.xepLoaiNV()}</td>
          <td>
              <button data-toggle="modal"
              data-target="#myModal" onclick="suaNhanVien('${
                nhanVien.taiKhoan
              }')" class="btn btn-warning">EDIT</button>
  
              <button onclick="xoaNhanVien('${
                nhanVien.taiKhoan
              }')" class="btn btn-danger">DELETE</button>
          </td>
      </tr>`;

    contentHTML += contentTr;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function xuatThongTinLenForm(dsnv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
